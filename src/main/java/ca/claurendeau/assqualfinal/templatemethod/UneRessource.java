package ca.claurendeau.assqualfinal.templatemethod;

public class UneRessource {

    public void open() {
        System.out.println("Open ressource");        
    }

    public void utilise() {
        System.out.println("Utilise ressource");        
    }

    public void close() {
        System.out.println("Close ressource");        
    }

}
