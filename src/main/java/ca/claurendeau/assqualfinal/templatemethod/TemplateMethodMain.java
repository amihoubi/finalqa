package ca.claurendeau.assqualfinal.templatemethod;

public class TemplateMethodMain {
    
    // Convertissez ce code à l'aide de remaniements que vous connaissez afin
    // d'utiliser adéquatement le template method.
    // Vous pouvez aussi convertir ce code à l'aide de la programmation fonctionnelle en Java.
    
    public static void main(String[] args) {
        
        UneRessource uneRessource = new UneRessource();
        try {
            uneRessource.open();
            uneRessource.utilise();
        } catch(Exception e) {
            System.out.println("Une Exception");
        } finally {
            uneRessource.close();
        }
    }

}
