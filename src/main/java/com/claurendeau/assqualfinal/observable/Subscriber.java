package com.claurendeau.assqualfinal.observable;

public interface Subscriber {
    void notifyMe(String event);
}
