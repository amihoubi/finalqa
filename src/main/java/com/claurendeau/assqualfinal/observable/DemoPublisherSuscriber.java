package com.claurendeau.assqualfinal.observable;

public class DemoPublisherSuscriber {
    
    public static void main(String[] args) {
        
        Publisher publisher = new Publisher();
        new SubscriberTwo(publisher);
        new SubscriberOne(publisher);
        
        publisher.sendEvent("Hello world!!!!");
        publisher.sendEvent("Hello étudiants!!!!");
        publisher.sendEvent("Hello folks!!!!");
        
    }
}
