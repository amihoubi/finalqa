package com.claurendeau.assqualfinal.observable;

public class SubscriberOne implements Subscriber {
    
    public SubscriberOne(Publisher observable) {
        observable.register(this, this);
    }

    @Override
    public void notifyMe(String event) {
        System.out.println(event.toLowerCase());
    }

}
