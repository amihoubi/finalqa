package com.claurendeau.assqualfinal.observable;

public class SubscriberTwo {

    public SubscriberTwo(Publisher publisher) {

        publisher.register(this, new Subscriber() {

            @Override
            public void notifyMe(String event) {
                System.out.println(event.toUpperCase());
            }
            
        });
    }
}
